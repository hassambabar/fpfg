// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
 importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');



 // Initialize Firebase
  var config = {
    apiKey: "AIzaSyCUk_Vqqb8VSW3BK_AP4H914dD3pZGwKmI",
    authDomain: "fpfg-45f99.firebaseapp.com",
    databaseURL: "https://fpfg-45f99.firebaseio.com",
    projectId: "fpfg-45f99",
    storageBucket: "fpfg-45f99.appspot.com",
    messagingSenderId: "188985084174"
  };
  firebase.initializeApp(config);
  const messaging = firebase.messaging();
// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = 'Background Message Title';
  var notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});
messaging.onMessage(function(payload) {
  console.log('Message received. ', payload);
  // ...
});
// [END background_handler]
