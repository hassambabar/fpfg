The Fridge module has been incorporated into the 4.x branch of Drupal for Facebook.

This module is no longer maintained.  Instead please check out the 4.x branch of http://drupal.org/project/fb.

See http://drupal.org/node/178857/git-instructions/7.x-4.x for details.

Sorry for the inconvenience, but the newer module will work better!
