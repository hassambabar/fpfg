<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup templates
 */
//echo "<pre>"; print_r($node); exit;
?>
<?php //echo "<pre>"; print_r($title); exit;

if (!empty($node->field_study_image['und'][0]['uri'])) {
  
  $imge_url =$node->field_study_image['und'][0]['uri'];

}else{
  $imge_url ='https://www.findpaidfocusgroup.com/fb-banner.png';
  
}

$current_url = $_SERVER['HTTP_HOST'] . request_uri();

?>

<script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "NewsArticle",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "<?php echo $current_url; ?>"
      },
      "headline": "<?php echo $title; ?>",
      "image": [
        "<?php echo $imge_url; ?>"
       ],
      "datePublished": "<?php print date('Y-m-d', $created); ?>",
      "dateModified": "<?php print date('Y-m-d', $created); ?>",
       "publisher": {
        "@type": "Organization",
        "name": "Focus Group",
        "logo": {
          "@type": "ImageObject",
          "url": "https://www.findpaidfocusgroup.com/logo.png"
        }
      }
    }
    </script>



<!-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5fc8b6483c5560e2"></script> -->
<!-- <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ((!$page && !empty($title)) || !empty($title_prefix) || !empty($title_suffix) || $display_submitted) : ?>
  <header>
   <h1> <?php print render($title); ?></h1>
  </header>
  <?php endif; ?> -->


<?php
// Hide comments, tags, and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);
//  print render($content);

?>
<div class="col-md-12 study-top-box">
  <div class="col-md-3"><span class="orange-heading">Payout</span> :<span class="orange-heading-blue"><?php echo $node->field_compensation_['und'][0]['value']; ?></span></div>

  <div class="col-md-3"><span class="orange-heading">Gender</span> :<span class="orange-heading-blue"><?php echo $node->field_study_gender['und'][0]['value']; ?></span></div>
  <div class="col-md-3"><span class="orange-heading">State</span> :<span class="orange-heading-blue"><?php echo $node->field_state['und'][0]['taxonomy_term']->name; ?></span></div>
  <div class="col-md-3"><span class="orange-heading">Published</span> :<span class="orange-heading-blue" ><?php print date('Y M d', $created); ?></span></div>

</div><br>

<div class='col-sm-10 col-md-12 col-lg-12' style="margin-top: 20px;">
  <?php
  if (!empty($node->field_study_image['und'][0]['uri'])) {
    //echo "<pre>"; print_r($node->field_study_image['und'][0]); exit;
    if (!empty($node->field_study_image['und'][0]['alt'])) {
      $img_alt = $node->field_study_image['und'][0]['alt'];
    } else {
      $img_alt = $title;
    }
    if (!empty($node->field_study_image['und'][0]['title'])) {
      $img_title = $node->field_study_image['und'][0]['title'];
    } else {
      $img_title = $title;
    }
    print theme('image_style', array('style_name' => 'original', 'alt' => $img_alt, 'title' =>  $img_title, 'path' => $node->field_study_image['und'][0]['uri']));
  }
  ?>
</div>


<div class="col-md-12">

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3792110030263830"
     data-ad-slot="3973230375"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

  <div class="body-study"> <?php print render($content['body']);  ?></div>
</div>
<div class="col-md-12">
  <span itemprop="author" itemscope itemtype="http://schema.org/Person" >Posted By <h5 itemprop="name" typeof="Person">FindPaidFocusGroup.com </h5></span>
</div>
<div class="col-md-12">
<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3792110030263830"
     data-ad-slot="3973230375"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>  

<a href="<?php echo $node->field_study_pre_screan['und'][0]['url']; ?>" target="_blank"><button type="button" class="btn btn-primary btn-block btn-apply-blue">CLICK HERE TO APPLY NOW</button></a>
<br>

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-3792110030263830"
     data-ad-slot="3973230375"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
<br>

<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-3792110030263830"
     crossorigin="anonymous"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="autorelaxed"
     data-ad-client="ca-pub-3792110030263830"
     data-ad-slot="8274573749"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

</div>
<!-- <div class="col-md-12 share-buttons">
  <div class="addthis_inline_share_toolbox"></div>

</div> -->


<?php
// Only display the wrapper div if there are tags or links.
$field_tags = render($content['field_tags']);
$links = render($content['links']);
if ($field_tags || $links) :
?>
  <footer>
    <?php print $field_tags; ?>
    <?php print $links; ?>
  </footer>
<?php endif; ?>
<?php //print render($content['comments']); 
?>
</article>