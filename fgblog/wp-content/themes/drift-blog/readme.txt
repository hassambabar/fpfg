=== Drift Blog ===

Contributors: candidthemes
Tags: two-columns, right-sidebar, custom-background, custom-colors, custom-menu, featured-images, theme-options, threaded-comments, translation-ready, blog


Requires at least: 4.5
Tested up to: 5.2
Stable tag: 1.0.5
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Drift Blog is the Child Theme of Gist. This theme is best ever crafted free WordPress theme for Blog, news and Magazine. It is a simple, easy to use, modern and creative, user friendly WordPress theme with typography, fonts and color options. In addition Drift Blog is responsive, cross browser compatible and child theme ready. Drift Blog comes with added custom widgets for social and author, sticky sidebar options, footer widget, sidebar options, meta option, copyright option, social options etc. 

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== License ===

Drift Blog is a child theme of Gist WordPress Theme, Copyright 2018 Candid Themes
Drift Blog is distributed under the terms of the GNU General Public License v2

License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Gist WordPress Theme, Copyright 2018 Candid Themes
Gist is distributed under the terms of the GNU General Public License v2

== Images Used in Screenshot ==
* https://pxhere.com/en/photo/1435391
* https://pxhere.com/en/photo/773727
* https://pxhere.com/en/photo/1430495


== Changelog ==

= 1.0.0 - April 28 2019 =
* Initial release