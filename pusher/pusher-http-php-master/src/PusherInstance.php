<?php

namespace Pusher;

class PusherInstance
{
    private static $instance = null;
    private static $app_id = '912597';
    private static $secret = '4d9d731b7520c6a75730';
    private static $api_key = 'e6620ce4312ada964253';

    /**
     * Get the pusher singleton instance.
     *
     * @return Pusher
     */
    public static function get_pusher()
    {
        if (self::$instance !== null) {
            return self::$instance;
        }

        self::$instance = new Pusher(
            self::$api_key,
            self::$secret,
            self::$app_id
        );

        return self::$instance;
    }
}
