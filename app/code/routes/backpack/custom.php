<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::get('api/article', 'App\Http\Controllers\Api\ArticleController@index');
Route::get('api/article-search', 'App\Http\Controllers\Api\ArticleController@search');
Route::get('api/article/{id}', 'App\Http\Controllers\Api\ArticleController@show');


Route::get('api/state', 'App\Http\Controllers\Api\StateController@GetStates');
Route::get('api/state_studies/{id}', 'App\Http\Controllers\Api\StudyController@GetStatesStudies');

Route::get('api/nationwide', 'App\Http\Controllers\Api\StudyController@GetNationwide');
Route::get('api/newfocusgroup', 'App\Http\Controllers\Api\StudyController@newfocusgroup');

Route::get('api/featuredstudies', 'App\Http\Controllers\Api\StudyController@featuredstudies');
Route::get('api/getSignleStudy/{id}', 'App\Http\Controllers\Api\StudyController@GetSingleStudy');



Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    // CRUD resources and other admin routes
    Route::crud('monster', 'MonsterCrudController');
    Route::crud('studies', 'MonsterCrudController');

    Route::crud('icon', 'IconCrudController');
    Route::crud('product', 'ProductCrudController');

    // ---------------------------
    // Backpack DEMO Custom Routes
    // Prevent people from doing nasty stuff in the online demo
    // ---------------------------
    if (app('env') == 'production') {
        // disable delete and bulk delete for all CRUDs
        $cruds = ['article','studies', 'category', 'tag', 'monster', 'icon', 'product', 'page', 'menu-item', 'user', 'role', 'permission'];
        foreach ($cruds as $name) {
            Route::delete($name.'/{id}', function () {
                return false;
            });
            Route::post($name.'/bulk-delete', function () {
                return false;
            });
        }
    }
    Route::crud('study', 'StudyCrudController');
    Route::crud('state', 'StateCrudController');
    Route::crud('city', 'CityCrudController');
}); // this should be the absolute last line of this file