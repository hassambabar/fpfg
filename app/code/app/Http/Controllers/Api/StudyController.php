<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Backpack\NewsCRUD\app\Models\Article;
use Illuminate\Http\Request;
use App\Models\Study;

class StudyController extends Controller
{
    public function index(Request $request)
    {
        $search_term = $request->input('q');

        if ($search_term) {
            $results = Article::where('state_id', 4)->paginate(10);
        } else {
            $results = Article::paginate(10);
        }

        return $results;
    }
    
    public function newfocusgroup(Request $request)
    {
        $search_term = $request->input('q');

        $results = Study::where('status', 1)->paginate(10);
        foreach ($results as $key => $singel) {
            // $results[$key]->state_name = $singel->GetState();
            $singel->GetState();

            if (!empty($singel->image)) {
                $results[$key]->img = \URL::to('') . '/storage/' . $singel->image;
            } else {
                $results[$key]->img = \URL::to('') . '/storage/study/image-placeholder.jpg';
            }

            if ($singel->gender == '0') {
                $results[$key]->gender = 'Both Male / Female';
            } elseif ($singel->gender == '1') {
                $results[$key]->gender = 'Male';
            } else {
                $results[$key]->gender = 'Female';
            }
        }

        return $results;
    }
    public function featuredstudies(Request $request)
    {
        $search_term = $request->input('q');

        $results = Study::where('status', 1)->where('featured', 1)->paginate(10);
        foreach ($results as $key => $singel) {
            // $results[$key]->state_name = $singel->GetState();
            $singel->GetState();

            if (!empty($singel->image)) {
                $results[$key]->img = \URL::to('') . '/storage/' . $singel->image;
            } else {
                $results[$key]->img = \URL::to('') . '/storage/study/image-placeholder.jpg';
            }

            if ($singel->gender == '0') {
                $results[$key]->gender = 'Both Male / Female';
            } elseif ($singel->gender == '1') {
                $results[$key]->gender = 'Male';
            } else {
                $results[$key]->gender = 'Female';
            }
        }

        return $results;
    }

    public function GetNationwide(Request $request)
    {
        $search_term = $request->input('q');

        $results = Study::where('nationwide', 1)->where('status', 1)->paginate(10);
        foreach ($results as $key => $singel) {
            // $results[$key]->state_name = $singel->GetState();
            $singel->GetState();

            if (!empty($singel->image)) {
                $results[$key]->img = \URL::to('') . '/storage/' . $singel->image;
            } else {
                $results[$key]->img = \URL::to('') . '/storage/study/image-placeholder.jpg';
            }

            if ($singel->gender == '0') {
                $results[$key]->gender = 'Both Male / Female';
            } elseif ($singel->gender == '1') {
                $results[$key]->gender = 'Male';
            } else {
                $results[$key]->gender = 'Female';
            }
        }

        return $results;
    }

    public function GetStatesStudies($id)
    {
      
        $results = Study::where('state_id', $id)->paginate(2);
        foreach ($results as $key => $singel) {
            // $results[$key]->state_name = $singel->GetState();
            $singel->GetState();

            if (!empty($singel->image)) {
                $results[$key]->img = \URL::to('') . '/storage/' . $singel->image;
            } else {
                $results[$key]->img = \URL::to('') . '/storage/study/image-placeholder.jpg';
            }

            if ($singel->gender == '0') {
                $results[$key]->gender = 'Both Male / Female';
            } elseif ($singel->gender == '1') {
                $results[$key]->gender = 'Male';
            } else {
                $results[$key]->gender = 'Female';
            }
        }

        return $results;
    }
    public function search(Request $request)
    {
        $term = $request->input('term');
        $options = Article::where('title', 'like', '%' . $term . '%')->get()->pluck('title', 'id');

        return $options;
    }

    
    public function GetSingleStudy($id)
    {
        $singel = Study::find($id);
        $singel->GetState();
        if($singel->gender == 0){
            $singel->gender = 'Both Male / Female';
        }elseif($singel->gender == 0){
            $singel->gender = 'Male';
        }else{
            $singel->gender = 'Female';
        }


        if (!empty($singel->image)) {
            $singel->img = \URL::to('') . '/storage/' . $singel->image;
        } else {
            $singel->img = \URL::to('') . '/storage/study/image-placeholder.jpg';
        }

        return $singel;
    }
    public function show($id)
    {
        return Article::find($id);
    }
}
