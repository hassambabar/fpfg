<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StudyRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\State;
use App\Models\City;

/**
 * Class StudyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StudyCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Study');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/study');
        $this->crud->setEntityNameStrings('study', 'studies');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
       // $this->crud->setFromDb();

       $this->crud->addColumn([
        'name'  => 'created_at',
        'label' => 'Create At',
        'type'  => 'datetime',
    ]);
    $this->crud->addColumn([
        'name'  => 'title',
        'label' => 'Title',
        'type'  => 'text',
    ]);

    $this->crud->addColumn([
        'name'  => 'category_id',
        'label' => 'Category',
        'type' => "model_function_attribute",
        'function_name' => 'getCategory', // the method in your Model
        'attribute' => 'name',
    ]);

    $this->crud->addColumn([
        'name'  => 'state_id',
        'label' => 'State',
        'type' => "model_function_attribute",
        'function_name' => 'GetState', // the method in your Model
         'attribute' => 'name',
    ]);


    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(StudyRequest::class);

        // TODO: remove setFromDb() and manually define Fields
       // $this->crud->setFromDb();

       CRUD::addField([   // Wysiwyg
        'name'  => 'title',
        'label' => 'Title',
        'type'  => 'text',
        'tab'   => 'Study Info',
    ]);

    CRUD::addField([   // Wysiwyg
        'name'  => 'slug',
        'label' => 'Slug',
        'type'  => 'text',
        'tab'   => 'Study Info',
    ]);

    CRUD::addField([   // Wysiwyg
        'name'  => 'content',
        'label' => 'Details',
        'type'  => 'wysiwyg',
        'tab'   => 'Study Info',
    ]);

    CRUD::addField([   // Wysiwyg
        'name'  => 'prescreen',
        'label' => 'Pre-Screen',
        'type'  => 'text',
        'tab'   => 'Study Info',
    ]);
    CRUD::addField([   // Wysiwyg
        'name'  => 'age',
        'label' => 'Age',
        'type'  => 'text',
        'tab'   => 'Study Info',
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],
    ]);
    
    $this->crud->addField([
        'name'    => 'gender', // the name of the db column
        'label'   => 'Gender', // the input label
        'type'    => 'radio',
        'options' => [ // the key will be stored in the db, the value will be shown as label;
            0 => 'Both Male / Female',
            1 => 'Male',
            2 => 'Female',
        ],
        // optional
        'inline' => true, // show the radios all on the same line?
        'tab'   => 'Study Info',
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],

    ]);

    $this->crud->addField([
        'name'    => 'nationwide', // the name of the db column
        'label'   => 'Nationwide', // the input label
        'type'    => 'radio',
        'options' => [ // the key will be stored in the db, the value will be shown as label;
            0 => 'No',
            1 => 'Yes',
         
        ],
        // optional
        'inline' => true, // show the radios all on the same line?
        'tab'   => 'Study Info',
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],

    ]);

    CRUD::addField([   // Wysiwyg
        'name'  => 'compensation',
        'label' => 'Compensation',
        'type'  => 'number',
        'tab'   => 'Study Info',
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],

    ]);


    $this->crud->addField([    // SELECT
        'label'     => 'Category',
        'type'      => 'select',
        'name'      => 'category_id',
        'entity'    => 'category',
        'attribute' => 'name',
        'model'     => "Backpack\NewsCRUD\app\Models\Category",
        'tab'   => 'Study Info',
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],

    ]);
    $this->crud->addField([    // SELECT
        'label'     => 'State',
        'type'        => 'select2_from_array',
        'name'      => 'state_id',
        'options'     => $this->states(),
        'tab'   => 'Study Info',
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],

    ]);

    $this->crud->addField([    // SELECT
        'label'     => 'City',
        'type'        => 'select2_from_array',
        'name'      => 'city_id',
        'options'     => $this->cities(),
        'tab'   => 'Study Info',
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],

    ]);


	$this->crud->addField([
        'name'   => 'image',
        'label'  => 'Image',
        'type'   => 'image',
        'upload' => true,
        'disk'   => 'public',
        'tab'   => 'Study Info',

    ]);

 


    $this->crud->addField([
        'name'    => 'status', // the name of the db column
        'label'   => 'Status', // the input label
        'type'    => 'radio',
        'options' => [ // the key will be stored in the db, the value will be shown as label;
            0 => 'Draft',
            1 => 'Published',
        ],
        // optional
        'inline' => true, // show the radios all on the same line?
        'tab'   => 'Study Info',
    ]);
    $this->crud->addField([
        'name'    => 'featured', // the name of the db column
        'label'   => 'Featured', // the input label
        'type'    => 'radio',
        'options' => [ // the key will be stored in the db, the value will be shown as label;
            0 => 'No',
            1 => 'Yes',
        ],
        // optional
        'inline' => true, // show the radios all on the same line?
        'tab'   => 'Study Info',
    ]);

    $this->crud->addField([   // Date
        'name'  => 'date',
        'label' => 'Date',
        'type'  => 'date_picker',
        // optional:
        'date_picker_options' => [
            'todayBtn' => true,
            'format'   => 'dd-mm-yyyy',
            'language' => 'en',
        ],
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],
        'tab'   => 'Study Info',

    ]);


    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function states()
	{
		$entries = State::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {

          //  echo "<pre>"; print_r($entry->id); exit;

			$tab[$entry->id] = $entry->name;
			
			
		}
		
		return $tab;
    }
    public function cities()
	{
		$entries = City::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {

          //  echo "<pre>"; print_r($entry->id); exit;

			$tab[$entry->id] = $entry->name;
			
			
		}
		
		return $tab;
	}
}
