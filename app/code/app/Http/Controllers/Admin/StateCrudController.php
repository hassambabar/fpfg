<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StateCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StateCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\State');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/state');
        $this->crud->setEntityNameStrings('state', 'states');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        $this->crud->setFromDb();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(StateRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        //$this->crud->setFromDb();

        CRUD::addField([   // Wysiwyg
            'name'  => 'name',
            'label' => 'Name',
            'type'  => 'text',
        ]);
       

        
        $this->crud->addField([
            'name'    => 'show_home', // the name of the db column
            'label'   => 'Show Home', // the input label
            'type'    => 'radio',
            'options' => [ // the key will be stored in the db, the value will be shown as label;
                0 => 'No',
                1 => 'Yes',
            ],
            // optional
            'inline' => true, // show the radios all on the same line?
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
    
        ]);
        $this->crud->addField([
            'name'    => 'show_menu', // the name of the db column
            'label'   => 'Show Menu', // the input label
            'type'    => 'radio',
            'options' => [ // the key will be stored in the db, the value will be shown as label;
                0 => 'No',
                1 => 'Yes',
            ],
            // optional
            'inline' => true, // show the radios all on the same line?
            'wrapperAttributes' => ['class' => 'form-group col-md-6'],
    
        ]);
        CRUD::addField([   // Wysiwyg
            'name'  => 'weight',
            'label' => 'weight',
            'type'  => 'number',
            'wrapperAttributes' => ['class' => 'form-group col-md-4'],
        ]);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
