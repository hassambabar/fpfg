<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CityRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\State;

/**
 * Class CityCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CityCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\City');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/city');
        $this->crud->setEntityNameStrings('city', 'cities');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
       // $this->crud->setFromDb();
       $this->crud->addColumn([
        'name'  => 'name',
        'label' => 'Name',
        'type'  => 'text',
    ]);

    
    $this->crud->addColumn([
        'name'  => 'state_id',
        'label' => 'State',
        'type' => "model_function_attribute",
        'function_name' => 'GetState', // the method in your Model
         'attribute' => 'name',
    ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CityRequest::class);

        // TODO: remove setFromDb() and manually define Fields
       // $this->crud->setFromDb();

       CRUD::addField([   // Wysiwyg
        'name'  => 'name',
        'label' => 'Name',
        'type'  => 'text',
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],

    ]);

    $this->crud->addField([    // SELECT
        'label'     => 'State',
        'type'        => 'select2_from_array',
        'name'      => 'state_id',
        'options'     => $this->states(),
        'wrapperAttributes' => ['class' => 'form-group col-md-6'],

    ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function states()
	{
		$entries = State::all();
		if ($entries->count() <= 0) {
			return [];
		}
		$tab = [];
		foreach ($entries as $entry) {

          //  echo "<pre>"; print_r($entry->id); exit;

			$tab[$entry->id] = $entry->name;
			
			
		}
		
		return $tab;
    }
}
